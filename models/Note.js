const mongoose = require('mongoose');
const joi = require('joi');

const NoteSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
}, {
  collection: 'notes',
  versionKey: false,
},
);

const Note = mongoose.model('Note', NoteSchema);

const validateNote = (note) => {
  const schema = joi.object({
    text: joi.string().empty().min(1).max(255).required()
        .messages({
          'string.base': `"text" should be a type of 'text'`,
          'string.empty': `"text" cannot be an empty field`,
          'string.min': `"text" should have a minimum length of {#limit}`,
          'string.max': `"text" should have a maximum length of {#limit}`,
          'any.required': `"text" is a required field`,
        })});

  return schema.validate(note);
};

module.exports = {Note, validateNote};
