const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const joi = require('joi');
const bcrypt = require('bcrypt');

const {Note} = require('./Note');

const {JWT_SECRET} = require('../config');

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
}, {
  collection: 'users',
},
);

UserSchema.virtual('notes', {
  ref: 'Note',
  localField: '_id',
  foreignField: 'userId',
});

UserSchema.pre('save', function(next) {
  if (!this.isModified('password')) {
    return next();
  }

  this.password = bcrypt.hashSync(this.password, 10);

  next();
});

UserSchema.pre('remove', async function(next) {
  await Note.deleteMany({userId: this.id});

  next();
});

UserSchema.methods.generateAuthToken = function() {
  return jwt.sign({_id: this._id}, JWT_SECRET);
};

UserSchema.methods.checkPassword = function(receivedPassword) {
  return bcrypt.compareSync(receivedPassword, this.password);
};

const User = mongoose.model('User', UserSchema);

const validateUser = (user) => {
  const schema = joi.object({
    username: joi.string().empty().min(1).max(50).required()
        .messages({
          'string.base': `"username" should be a type of 'text'`,
          'string.empty': `"username" cannot be an empty field`,
          'string.min': `"username" should have a minimum length of {#limit}`,
          'string.max': `"username" should have a maximum length of {#limit}`,
          'any.required': `"username" is a required field`,
        }),
    password: joi.string().min(1).max(255).required()
        .messages({
          'string.base': `"password" should be a type of 'text'`,
          'string.empty': `"password" cannot be an empty field`,
          'string.min': `"password" should have a minimum length of {#limit}`,
          'string.max': `"password" should have a maximum length of {#limit}`,
          'any.required': `"password" is a required field`,
        }),
  });

  return schema.validate(user);
};

module.exports = {User, validateUser};
