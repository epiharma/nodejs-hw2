const express = require('express');
const router = new express.Router();

const {
  register,
  login,
} = require('../controllers/authController.js');

const {asyncWrapper} = require('../utils/helpers.js');

router.post('/register', asyncWrapper(register));
router.post('/login', asyncWrapper(login));

module.exports = router;
