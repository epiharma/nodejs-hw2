const express = require('express');
const router = new express.Router();

const {isAuthorized} = require('../middlewares/authMiddleware.js');

const {
  getNotes,
  addNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
} = require('../controllers/noteController.js');

const {asyncWrapper} = require('../utils/helpers.js');

router.get('/', isAuthorized, asyncWrapper(getNotes));
router.post('/', isAuthorized, asyncWrapper(addNote));
router.get('/:id', isAuthorized, asyncWrapper(getNote));
router.put('/:id', isAuthorized, asyncWrapper(updateNote));
router.patch('/:id', isAuthorized, asyncWrapper(checkNote));
router.delete('/:id', isAuthorized, asyncWrapper(deleteNote));

module.exports = router;
