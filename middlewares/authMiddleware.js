const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {BadRequestError, UnauthorizedError} = require('../utils/errors');

const isAuthorized = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    throw new UnauthorizedError(`No Authorization http header found!`);
  }

  let [tokenType, token] = header.split(' ');

  if (!token && !tokenType) {
    throw new UnauthorizedError(`No JWT token found!`);
  }

  if (!token) {
    // crutches are our everything
    token = tokenType;
  }

  try {
    req.userId = jwt.verify(token, JWT_SECRET)._id;

    next();
  } catch (e) {
    throw new BadRequestError('Invalid token');
  }
};

module.exports = {
  isAuthorized,
};
