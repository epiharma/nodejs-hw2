module.exports = {
  ...require('./config'),
  ...require('./database'),
};
