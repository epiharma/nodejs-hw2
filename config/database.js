const {
  DB_USERNAME,
  DB_PASSWORD,
  DB_CLUSTER,
  DB_NAME,
} = require('./config.js');

module.exports = {
  MONGO_URI: `mongodb+srv://${DB_USERNAME}:${DB_PASSWORD}@${DB_CLUSTER}.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`,
};
