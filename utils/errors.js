/** * @class Represents Bad Request Error */
class BadRequestError extends Error {
  /**
     * @param {string} message The message to be sent in json.
     */
  constructor(message = 'Bad request') {
    super(message);
    this.statusCode = 400;
  }
}

/** * @class Represents Unauthorized Error */
class UnauthorizedError extends Error {
  /**
   * @param {string} message The message to be sent in json.
   */
  constructor(message = 'Unauthorized user!') {
    super(message);
    this.statusCode = 401;
  }
}

module.exports = {
  BadRequestError,
  UnauthorizedError,
};
