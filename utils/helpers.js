const asyncWrapper = (callback) => {
  return (req, res, next) => {
    callback(req, res, next).catch(next);
  };
};

const isUserNote = (note, userId) => {
  return String(note.userId) === String(userId);
};

const errorHandler = (err, req, res, next) => {
  if (err.statusCode) {
    return res.status(err.statusCode).json({message: err.message});
  }

  return res.status(500).json({message: err.message});
};

module.exports = {asyncWrapper, isUserNote, errorHandler};
