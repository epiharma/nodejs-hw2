const {User, validateUser} = require('../models/User.js');
const {BadRequestError} = require('../utils/errors');

const register = async (req, res) => {
  const {username, password} = req.body;
  const validationResult = validateUser({username, password});

  if (validationResult.error) {
    throw new BadRequestError(validationResult.error.message);
  }

  let user = await User.findOne({username});

  if (user) {
    throw new BadRequestError('This username is used');
  }

  user = new User({username, password});
  await user.save();

  return res.status(200).json({message: 'Success'});
};

const login = async (req, res) => {
  const {username, password} = req.body;

  if (!username || !password) {
    throw new BadRequestError('Enter your credentials');
  }

  const user = await User.findOne({username});

  if (!user) {
    throw new BadRequestError(`Non-existing username`);
  }

  if (!user.checkPassword(password)) {
    throw new BadRequestError(`Wrong password`);
  }

  const token = user.generateAuthToken();

  return res.status(200).json({message: 'Success', jwt_token: token});
};

module.exports = {
  register,
  login,
};
