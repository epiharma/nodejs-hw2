const {isUserNote} = require('../utils/helpers');
const {BadRequestError} = require('../utils/errors');

const {Note, validateNote} = require('../models/Note.js');

const getNotes = async (req, res) => {
  const {userId} = req;
  let {limit, offset} = req.query;

  offset = Number(offset);
  limit = Number(limit);

  const notes = await Note.find({userId}).skip(offset).limit(limit);

  return res.status(200).json({notes});
};

const addNote = async (req, res) => {
  const {userId} = req;
  const {text} = req.body;

  const validationResult = validateNote({text});

  if (validationResult.error) {
    throw new BadRequestError(validationResult.error.message);
  }

  const note = new Note({text, userId});
  await note.save();

  return res.status(200).json({message: 'Success'});
};

const getNote = async (req, res) => {
  const {id} = req.params;
  const {userId} = req;

  const note = await Note.findById(id).exec();

  if (!note) {
    throw new BadRequestError('Note is not found');
  }

  if (!isUserNote(note, userId)) {
    throw new BadRequestError('You can\'t delete others notes');
  }

  return res.status(200).json({note});
};

const updateNote = async (req, res) => {
  const {id} = req.params;
  const {text} = req.body;
  const {userId} = req;

  const validationResult = validateNote({text});

  if (validationResult.error) {
    throw new BadRequestError(validationResult.error.message);
  }

  const note = await Note.findById(id).exec();

  if (!note) {
    throw new BadRequestError('Note is not found');
  }

  if (!isUserNote(note, userId)) {
    throw new BadRequestError('You can\'t delete others notes');
  }

  note.text = text;
  await note.save();

  return res.status(200).json({message: 'Success'});
};

const checkNote = async (req, res) => {
  const {id} = req.params;
  const {userId} = req;

  const note = await Note.findById(id).exec();

  if (!note) {
    throw new BadRequestError('Note is not found');
  }

  if (!isUserNote(note, userId)) {
    throw new BadRequestError('You can\'t delete others notes');
  }

  note.completed = !note.completed;
  await note.save();

  return res.status(200).json({message: 'Success'});
};

const deleteNote = async (req, res) => {
  const {id} = req.params;
  const {userId} = req;

  const note = await Note.findById(id).exec();

  if (!note) {
    throw new BadRequestError('Note is not found');
  }

  if (!isUserNote(note, userId)) {
    throw new BadRequestError('You can\'t delete others notes');
  }

  await note.remove();

  return res.status(200).json({message: 'Success'});
};

module.exports = {
  getNotes,
  addNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
};
