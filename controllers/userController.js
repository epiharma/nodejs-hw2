const {User} = require('../models/User');
const {BadRequestError} = require('../utils/errors');

const getProfileInfo = async (req, res) => {
  const user = await User.findById(req.userId);

  if (!user) {
    throw new BadRequestError('No user found');
  }

  return res.status(200).json({
    user: {
      _id: user._id,
      username: user.username,
      createdDate: user.createdDate,
    },
  });
};

const deleteProfile = async (req, res) => {
  const user = await User.findById(req.userId);

  await user.remove();

  return res.status(200).json({message: 'Success'});
};

const changeProfilePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;

  if (!oldPassword || !newPassword) {
    throw new BadRequestError('Password can\'t be empty');
  }

  const user = await User.findById(req.userId);
  const passwordsMatched = await user.checkPassword(oldPassword);

  if (!passwordsMatched) {
    throw new BadRequestError('Wrong old password');
  }

  user.password = newPassword;
  await user.save();

  return res.status(200).json({message: 'Success'});
};

module.exports = {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
};
